# _synorders_

## Чем занимается?

> Добавляет представление для заказа cml_orders и отображение страницы с заказами (/orders). Страница позволяет фильтровать заказы по ряду параметров.
> Позволяет выгрузить заказы в формате XLS.

## Требования к платформе

- _Drupal 8-10_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/synorders)

```sh
composer require 'drupal/synorders'
```

- [Drupal.org dev версия](https://www.drupal.org/project/synorders/releases/8.x-1.x-dev)

```sh
composer require 'drupal/synorders:1.x-dev@dev'
```

## Как использовать?

- Дополнительных настроек не требуется
