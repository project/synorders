<?php

namespace Drupal\synorders\Hook;

/**
 * @file
 * Contains \Drupal\synusers\Hook.
 */

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Controller routines for page example routes.
 */
class ViewsPreView {

  /**
   * Hook.
   */
  public static function hook($view, $display_id, &$args) {
    if ($view->id() == 'cml_orders' && $display_id == 'page') {
      $option = [
        'query' => $view->getExposedInput(),
      ];
      $link = Link::fromTextAndUrl(t('Download Excel'), Url::fromUri('internal:/orders-xls', $option))->toString();
      $options = [
        'id' => 'area_text_custom',
        'table' => 'views',
        'field' => 'area_text_custom',
        'relationship' => 'none',
        'group_type' => 'none',
        'admin_label' => '',
        'empty' => TRUE,
        'tokenize' => FALSE,
        'content' => '<div class="load-xls">' . $link . '</div>',
        'plugin_id' => 'text_custom',
      ];
      $view->setHandler('page', 'header', 'area_text_custom', $options);
    }
  }

}
